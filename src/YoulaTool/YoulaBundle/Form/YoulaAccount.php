<?php

namespace YoulaTool\YoulaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class YoulaAccount extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("uid", TextType::class)
            ->add("adv_id", TextType::class)
            ->add("app_id", TextType::class)
            ->add("usr_latitude", TextType::class)
            ->add("usr_longitude", TextType::class)
            ->add('phone', TextType::class)
            ->add('token', TextType::class)
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

}
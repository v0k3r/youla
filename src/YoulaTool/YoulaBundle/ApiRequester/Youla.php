<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vlad <vladislav.baymurzin@i-a-t.net>  
 */

namespace YoulaTool\YoulaBundle\ApiRequester;


use YoulaTool\YoulaBundle\DTO\YoulaAccount;

class Youla extends ApiRequester implements YoulaInterface
{

    /**
     * If your need to create new account in youla
     * @param $lat
     * @param $long
     * @return YoulaAccount
     */
    public function makeAccount($lat, $long)
    {
        $arr = $this->youlaHelper->generateAcc($lat, $long);
        $acc = new YoulaAccount();
        $acc->_mapFromArray($arr);
        return $acc;
    }

    public function authByNumber(YoulaAccount $account, $phone)
    {
        $response = $this->request(
            "/api/v1/auth/phone",
            "POST",
            [
                'query' => $this->youlaHelper->accountToQueryArray($account),
                'form_params' => [
                    'uid' => $account->uid,
                    'phone' => $phone
                ]
            ]
        );

        return $response;
    }

    public function confirmAccount(YoulaAccount $account, $phone, $code)
    {
        $response = $this->request(
            "/api/v1/auth/confirm",
            "POST",
            [
                'query' => $this->youlaHelper->accountToQueryArray($account),
                'form_params' => [
                    'uid' => $account->uid,
                    'phone' => $phone,
                    'code' => $code
                ]
            ]);
        return $response;
    }

    public function getProfileInfo(YoulaAccount $account)
    {
        $response = $this->request(
            '/api/v1/user/' . $account->acc_id,
            "GET",
            [
                'headers' => [
                    'User-Agent' => 'Youla/1.14.2 (iOS Version 10.2 (Build 14C92))',
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json; charset=utf-8',
                    'X-Auth-Token' => $account->token,
                    'Accept-Language' => 'ru-RU;q=1',
                    'Accept-Encoding' => 'gzip, deflate'
                ],
                'query' => $this->youlaHelper->accountToQueryArray($account)
            ]);
        return $response;
    }

    public function updateProfileInfo(YoulaAccount $account, $last_name, $name)
    {
        $response = $this->request(
            '/api/v1/user/' . $account->acc_id,
            "POST",
            [
                'headers' => [
                    'User-Agent' => 'Youla/1.14.2 (iOS Version 10.2 (Build 14C92))',
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json; charset=utf-8',
                    'X-Auth-Token' => $account->token,
                    'X-HTTP-Method-Override' => 'PUT',
                    'Accept-Language' => 'ru-RU;q=1',
                    'Accept-Encoding' => 'gzip, deflate'
                ],
                'query' => $this->youlaHelper->accountToQueryArray($account),
                'json' => [
                    'email' => null,
                    "last_name" => $last_name,
                    'settings' =>
                        array(
                            'display_phone' => false,
                            'favorite_push_enabled' => true,
                            'product_status_push_enabled' => true,
                            'messages_push_enabled' => true,
                            'location' =>
                                array(
                                    'latitude' => null,
                                    'longitude' => null,
                                    'description' => null,
                                ),
                        ),
                    'image' => null,
                    'first_name' => $name
                ]
            ]);
        return $response;
    }
}
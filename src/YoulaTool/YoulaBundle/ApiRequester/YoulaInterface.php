<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 10.03.2017
 * Time: 1:38
 */

namespace YoulaTool\YoulaBundle\ApiRequester;
use YoulaTool\YoulaBundle\DTO\YoulaAccount;

/**
 * Class YoulaInterface
 * @package YoulaTool\YoulaBundle\ApiRequester
 */
interface YoulaInterface
{
    /**
     * @param $lat
     * @param $long
     * @return YoulaAccount
     */
    public function makeAccount($lat, $long);

    public function authByNumber(YoulaAccount $account, $phone);

    public function confirmAccount(YoulaAccount $account, $phone, $code);

    public function getProfileInfo(YoulaAccount $account);

    public function updateProfileInfo(YoulaAccount $account, $last_name, $name);

}
<?php

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 10.03.2017
 * Time: 0:49
 */

namespace YoulaTool\YoulaBundle\ApiRequester;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use YoulaTool\YoulaBundle\Services\YoulaHelper;

/**
 * Class ApiRequester
 * @package YoulaTool\YoulaBundle\ApiRequester
 */
class ApiRequester
{
    /**
     * GuzzleHTTP Client
     * @var Client
     */
    protected $client;

    /**
     * Helper
     * @var YoulaHelper
     */
    protected $youlaHelper;

    /**
     * APIRequester constructor.
     * @param Client $client Guzzle Http Client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param YoulaHelper $youlaHelper
     */
    public function setYoulaHelper(YoulaHelper $youlaHelper)
    {
        $this->youlaHelper = $youlaHelper;
    }



    /**
     * Common method to make requests to the API
     * @param string $url Request URL
     * @param string $method Request method
     * @param array $formParams form params
     * @return mixed API response
     * @throws RequestException if request is bad
     */
    protected function request($url, $method, $params = [], $errorMessage = null, $needJsonResponse = true)
    {

        try {
            $response = $this->client->request($method, $url, $params);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse()->getBody()->getContents();
            } else {
                return $this->prepareExceptionResponse($e);
            }
        }
        if ($needJsonResponse) {
            try {
                $json = $this->getResponseJSON($response);
            } catch (\Exception $e) {
                return $this->prepareExceptionResponse($e);
            }
        } else {
            return $response->getBody()->getContents();
        }
        return $json;
    }

    protected function prepareExceptionResponse($e) {
        return [

        ];
    }

    /**
     * @param $response ResponseInterface
     * @return array|null Data returned from API or null on fail
     */
    protected function getResponseJSON($response)
    {
        $json = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        if (!$json) {
            return null;
        }
        return $json;
    }

}
<?php

namespace YoulaTool\YoulaBundle\Services;

use YoulaTool\YoulaBundle\DTO\YoulaAccount;

/**
 * Class YoulaHelper
 * @package YoulaTool\YoulaBundle\Services
 */
class YoulaHelper
{
    public function generateAcc($lat, $long) {
        return [
            "adv_id" =>  $this->randomAdvId(),
            "app_id" => $this->randomAppId(),
            "uid" => $this->randomUid(),
            "usr_latitude" => $lat,
            "usr_longitude" => $long
        ];
    }

    private function randomAdvId() {
        //46EF468C-BB8A-4E27-B875-428DFCAB455D
        $part1 = rand(6300, 9700);
        return "acbd9259-4901-4bb9-{$part1}-4d0" . self::randomStr(9);
    }

    private function randomStr($l = 0, $dig = false) {
        if ($l == 0) return "";
        $digs = "0123456789";
        $str = "0123456789abcdefghijklmnopqrstuvwxyz";
        return substr(str_shuffle(($dig) ? $digs : $str), 0, $l);
    }

    private function randomUid() {
        //405D4BB8-68BE-412E-B785-577CBF2FC4FB
        return "8b0b68". str_shuffle("e9fdead9ac");
    }

    private function randomAppId() {
        return rand(0,1) ? "iphone/" . rand(230, 3800) : "android/" . rand(230, 3800);
    }

    public function accountToQueryArray(YoulaAccount $account) {
        return [
            'app_id' => $account->app_id,
            'uid' => $account->uid,
            'usr_latitude' => $account->usr_latitude,
            'usr_longitude' => $account->usr_longitude,
            'adv_id' => $account->adv_id,
            "po_bratsky" => 1,
            'timestamp' => time()
        ];
    }
}
<?php

namespace YoulaTool\YoulaBundle\DTO;

/**
 * Class YoulaAccount
 * @package YoulaTool\YoulaBundle\DTO
 */
class YoulaAccount
{
    public $uid;
    public $adv_id;
    public $app_id;
    public $usr_latitude;
    public $usr_longitude;
    public $po_bratsky = 1;
    public $timestamp;
    public $phone = null;
    public $token = null;
    public $acc_id = null;


    public function _mapFromArray(array $data = array()) {
        foreach($data as $key => $value){
            $this->{$key} = $value;
        }
    }

}
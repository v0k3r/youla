<?php

/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.04.17
 * Time: 15:09
 */
namespace AppBundle\Command;

use AppBundle\Entity\Dispatch;
use AppBundle\Entity\SubText;
use AppBundle\Entity\Template;
use AppBundle\Entity\TimeToPublish;
use AppBundle\Entity\YoulaAccount;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class MyCommand extends ContainerAwareCommand
{


    protected function configure()
    {

        $this
            ->setName('app:create-dispatch')
            ->setDescription('Creates a new dispatch.')
            ->setHelp('This command allows you to create a dispatch...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $time = new \DateTime('now');
        $time = $time->format("H:i");



        $templates = $em->getRepository('AppBundle:Template')->findBy(['status' => true]);
        foreach ($templates as $template) {

            $accounts = $template->getYoulaAccount();

            if (count($accounts)==0) {

                continue;
            }
            $times = $template->getTimeToPublish();
            $publishTime=$em->getRepository('AppBundle:TimeToPublish')->findOneBy(['name'=>$time]);


            if ($publishTime &&$times->contains($publishTime)) {

               $youlaAccount=$accounts[0];

                $dispatch = new Dispatch();
                $dispatch
                    ->setTemplate($template)
                    ->setText($this->getNextRandomText($em,$template))
                    ->setPublishedTime(new \DateTime('now'))
                    ->setYoulaAccount($youlaAccount);
                $em->persist($dispatch);
                $youlaAccount->setLastPublishedDate(new \DateTime('now'));
                $em->persist($youlaAccount);
                $em->flush();

            }

        }


    }

    private function getNextRandomText(EntityManager $em, Template $template)
    {


        $texts = $em->getRepository('AppBundle:SubText')->findBy(['parent' => $template]);
        $disp = $em->getRepository('AppBundle:Dispatch')->findBy(['template' => $template]);
        $t = array();
        foreach ($disp as $d)
            $t[] = $d->getText();
        $out = array_diff($texts, $t) ?: [$texts[mt_rand(0, count($texts) - 1)]];
        return (array_shift($out));
    }
// */1 * * * * cd /home/kolya/PhpstormProjects/youlatool && php bin/console app:create-dispatch »logfile.txt
}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 14.04.17
 * Time: 0:11
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DatagridBundle\ProxyQuery\ProxyQueryInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'user';

    public function configureBatchActions($actions)
    {
        if (
            $this->hasRoute('edit') && $this->hasAccess('edit') &&
            $this->hasRoute('delete') && $this->hasAccess('delete')
        ) {
            $actions['message'] = array(
                'ask_confirmation' =>  true
            );

        }

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('Content', array('class' => 'col-md-9'))
            ->add('username')
            ->add('email', 'textarea')
            ->add('password', 'text')
            ->add('UserRoles', 'sonata_type_model', array(
                'multiple' => true,
                'class' => 'AppBundle\Entity\Role',
                'property' => 'name',
            ))

            ->end();


    }

    protected function configureListFields(ListMapper $listMapper)
    {


        $listMapper
            ->addIdentifier('username')
            ->addIdentifier('email')
            ->add('userRoles')
            ->add('user.transaction')
           ;
    }

    public function preUpdate($object)
    {
        $pass = $this->getConfigurationPool()->getContainer()->get('security.password_encoder')->encodePassword($object, $object->getPassword());
        $object->setPassword($pass);
    }

    public function prePersist($object)
    {
        $pass = $this->getConfigurationPool()->getContainer()->get('security.password_encoder')->encodePassword($object, $object->getPassword());
        $object->setPassword($pass);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('userRoles', null, array(), 'entity', array(
                'class' => 'AppBundle\Entity\Role',
                'choice_label' => 'name',
            ));

    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'batch_confirmation':
                return  "CRUD/batch_confirmation.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 17.04.17
 * Time: 16:33
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ExpectedUserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email');
    }
    public function configureBatchActions($actions)
    {

        if (
            $this->hasRoute('edit') && $this->hasAccess('edit') &&
            $this->hasRoute('delete') && $this->hasAccess('delete')
        ) {
            $actions['message'] = array(
                'ask_confirmation' =>  new RedirectResponse('list')
            );

        }

        return $actions;
    }
    public function getTemplate($name)
    {
        switch ($name) {
            case 'batch_confirmation':
                return  "CRUD/batch_confirmation.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
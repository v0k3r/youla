<?php

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:23
 */

namespace AppBundle\Controller\Traits;
use AppBundle\Services\Interfaces\ControllerAsServiceInterface;


/**
 * Class ControllerAsServiceSetter
 * @package AppBundle\Controller\Traits
 */
trait ControllerAsServiceSetter
{


    /**
     * @var ControllerAsServiceInterface Service to replace base methods in controllers which used as service
     */
    protected $controllerAsService;

    /**
     * Social networks features service setter
     *
     * @param ControllerAsServiceInterface $controllerAsService Service to replace base methods in controllers which used as service
     */
    public function setControllerAsService(ControllerAsServiceInterface $controllerAsService)
    {
        $this->controllerAsService = $controllerAsService;
    }

    /**
     * Returns ControllerAsService service
     *
     * @return ControllerAsServiceInterface
     */
    public function getControllerAsService()
    {
        return $this->controllerAsService;
    }
}
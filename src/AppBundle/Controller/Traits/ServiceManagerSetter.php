<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:25
 */

namespace AppBundle\Controller\Traits;
use AppBundle\Services\Interfaces\ServiceManagerInterface;
use AppBundle\Services\ServiceManager;

/**
 * Class ServiceManager
 * @package AppBundle\Controller\Traits
 */
trait ServiceManagerSetter
{
    /**
     * @var ServiceManagerInterface service
     */
    protected $serviceManager;

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param ServiceManagerInterface $serviceManager
     */
    public function setServiceManager(ServiceManagerInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }


}
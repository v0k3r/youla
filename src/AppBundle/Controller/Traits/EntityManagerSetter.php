<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:34
 */

namespace AppBundle\Controller\Traits;



use AppBundle\Services\EntityManagerService;
use AppBundle\Services\Interfaces\EntityManagerServiceInterface;

trait EntityManagerSetter
{
    /**
     * @var EntityManagerServiceInterface em service
     */
    protected $em;

    /**
     * @return EntityManagerService
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param EntityManagerServiceInterface $em
     */
    public function setEm(EntityManagerServiceInterface $em)
    {
        $this->em = $em;
    }



}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 25.03.17
 * Time: 11:45
 */

namespace AppBundle\Controller;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Class ErrorsController
 * @Route(service="app.errors_controller")
 * @package AppBundle\Controller
 */
class ErrorsController
{
    use ServiceManagerSetter;

    /**
     * @Route("/error/403", name="errors_page")
     *
     */
    public function errorAction(Request $request)
    {


        return $this->getServiceManager()->getControllerAsService()->render('page/errors/403.html.twig', array(

        ));
    }

}
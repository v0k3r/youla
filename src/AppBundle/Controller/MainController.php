<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:58
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Traits\ServiceManagerSetter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MainController
 * @package AppBundle\Controller
 * @Route(service="app.main_controller")
 */
class MainController extends Controller
{

    use ServiceManagerSetter;

    /**
     * @Route("/", name="mainpage")
     */
    public function indexAction() {
        return $this->getServiceManager()->getControllerAsService()->render(
            "layout/index.html.twig"
        );
    }
}
<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Controller;


use AppBundle\Controller\Traits\ServiceManagerSetter;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountController
 * @package AppBundle\Controller
 * @Route(service="app.account_controller")
 */
class AccountController extends Controller
{
    use ServiceManagerSetter;


    /**
     * @Route("accounts/getAll", name="accounts-get-all")
     * @param Request $request
     * @return Response|JsonResponse|null
     */
    public function getAllAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $em->getRepository("AppBundle:YoulaAccount")->findBy(['user' => $this->getServiceManager()->getControllerAsService()->getUser()]);
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json', $context);
        return new Response($output);
    }

    /**
     * @Route("accounts/store", name="accounts-store")
     * @param Request $request
     */
    public function store(Request $request)
    {

    }
}
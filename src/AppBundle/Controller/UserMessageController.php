<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 09.05.17
 * Time: 13:26
 */

namespace AppBundle\Controller;
use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Class UserMessageController
 * @Route(service="app.usermessage_controller")
 * @package AppBundle\Controller
 */
class UserMessageController extends Controller
{
    use ServiceManagerSetter;
    use EntityManagerSetter;

    /**
     * @Rest\Get("/messages/paginate", name="messages_paginate")
     */
    public function paginateAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $this->getServiceManager()->getControllerAsService()->getUser()->getMessages();
        foreach ($output as &$ot) {
            if ($ot == null) {
                return new RedirectResponse(
                    $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
                );
            }
        }
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json');
        return new Response($output);
    }
    /**
     * @Route("/messages")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {

        return $this->getServiceManager()->getControllerAsService()->render("page/message/list.html.twig");
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 10:55
 */

namespace AppBundle\Controller;


use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

/**
 * Class RegistrationController
 * @package AppBundle\Controller
 */
class RegistrationController extends Controller
{


    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }


    /**
     * @Route("/register", name="user_registration")
     *
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this
                ->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $user->getUserRoles()->add($em->getRepository('AppBundle:Role')->findOneByName("ROLE_GUEST"));
            $em->persist($user);
            $em->flush();
            $this->authenticateUser($user);


            $httpClient = new GuzzleAdapter(new Client());
            $sparky = new SparkPost($httpClient, ['key' => '5cf4e8b650f94f1360dc47510dce465516036f00']);
            $sparky->setOptions(['async' => false]);
            $hashKey=md5($user->getId()).md5($user->getUsername());
            try {
                $results = $sparky->transmissions->post([
                    'options' => [
                        'sandbox' => true
                    ],
                    'content' => [
                        'from' => 'yoola-tool@sparkpostbox.com',
                        'subject' => 'Ссылка на подтверждение аккаунта!',
                        'html' => $this->renderView('page/mail/confirm.html.twig',['hash'=>$hashKey])
                    ],
                    'recipients' => [
                        ['address' => ['email'=>$user->getEmail()]]
                    ]
                ]);
                $errorMail='На почту вам отправлено подтверждение';
            } catch (\Exception $err) {
                $errorMail='С вашей почтой то-то не так. Обратитесь к администратору';

            }


            return $this->render(
                'page/mail/info.html.twig',
                array(
                    'info' => $errorMail
                )
            );


            return $this->redirectToRoute('accounts_index');
        }

        return $this->render('auth/register.html.twig', array(
            'form' => $form->createView()
        ));
    }




}
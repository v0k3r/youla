<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 19.03.17
 * Time: 23:24
 */

namespace AppBundle\Controller;

use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use AppBundle\Entity\Transaction;
use Symfony\Component\Validator\Constraints\DateTime;
use AppBundle\Entity\UserPlan;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;

/**
 * Class PaymentController
 * @Route(service="app.payment_controller")
 * @package AppBundle\Controller
 */
class PaymentController extends Controller
{
    use ServiceManagerSetter;
    use EntityManagerSetter;

    /**
     * @Route("/payment", name="payment")
     */
    public function indexAction(Request $request)
    {

        $errors = '';
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $user = $this->getServiceManager()->getControllerAsService()->getUser();
        $plans = $em->getRepository('AppBundle:Plan')->findAll();
        foreach ($plans as $k => $plan) {

            if (!is_null($request->request->get($k))) {
                if ($user->getCash() < $plan->getCost()) {
                    $errors = 'Недостаточно денег. Пополните счет';


                } else {
                    if (is_null($user->getPlan())) {

                        $up = new UserPlan();
                        $up->setUser($user)->setPlan($plan);
                        $em->persist($up);

                    } else {
                        $user->getPlan()->setPlan($plan)->setStartTime(new \DateTime('now'))->setEndTime(new \DateTime('+30 days'));

                    }
                    $tr = new Transaction();
                    $tr->setUser($user)->setSumm(-($plan->getCost()))->setStatus('Все норм')->setDescription('Покупка тарифа "' . $plan->getName() . '"');

                    $user->setCash(-($plan->getCost()));
                    $em->persist($tr);
                    $em->flush();

                    return new RedirectResponse(
                        $this->getServiceManager()->getControllerAsService()->generateUrl("payment")
                    );
                }

            }
        }


        return $this->getServiceManager()->getControllerAsService()->render(
            'page/payment/payment.html.twig',
            ['plans' => $plans,
                'errors' => $errors]

        );
    }

    /**
     * @Rest\Get("/payment/paginate", name="payment_paginate")
     */
    public function paginateAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $this->getServiceManager()->getControllerAsService()->getUser()->getTransaction();
        foreach ($output as &$ot) {
            if ($ot == null) {
                return new RedirectResponse(
                    $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
                );
            }
        }
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json');
        return new Response($output);
    }

    /**
     * @Route("/pay/generate", name="generate-pay")
     * @param Request $request
     * @return  JsonResponse|Response|RedirectResponse
     */
    public function generatePaymentUrlAction(Request $request)
    {
        $sum = $request->get("sum");
        if (!preg_match("/^[0-9]*$/", $sum)) {
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl("payment")
            );
        }
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $user = $this->getServiceManager()->getControllerAsService()->getUser();
        $customParams = [];

        $transaction = new Transaction();
        $transaction
            ->setUser($user)
            ->setSumm($sum)
            ->setStatus('Ожидает оплаты')
            ->setDescription('Пополнение баланса');

        $em->persist($transaction);
        $em->flush($transaction);

        $customParams['shp_email'] = $user->getEmail();


        $url = $this->getServiceManager()->getRobokassa()->buildUrl([
            'outSum' => $sum,
            'invId' => $transaction->getId(),
            'invDesc' => "Пополнение баланса",
            'email' => $user->getEmail()
        ], $customParams);


        return new RedirectResponse(
            $url
        );
    }

    /**
     * @Route("/payment/fail", name="pay-fail")
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function onFailAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $id = $request->get('InvId');
        $logger = $this->getServiceManager()->getLogger();
        $order = $em->getRepository("AppBundle:Transaction")->findOneById($id);
        $logger->info("REQUEST onFailAction(): ", $request->request->all());

        $order->setStatus("Ошибка");
        $em->flush();
        $em->clear();
        $plans = $em->getRepository('AppBundle:Plan')->findAll();
        return $this->getServiceManager()->getControllerAsService()->render(
            'page/payment/payment.html.twig',
            ['plans' => $plans,
                'errors' => 'Ошибка платежа']

        );
    }

    /**
     * @Route("/payment/success")
     * @param Request $request
     * @return Response
     */
    public function onSuccess(Request $request)
    {
        return new Response("Оплата прошла успешно");
    }


    /**
     * @Route("/payment/result")
     * @param Request $request
     * @return Response
     */
    public function onResult(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $robokassa = $this->getServiceManager()->getRobokassa();
        $tm = getdate(time() + 9 * 3600);
        $date = "$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";
        $requestData = $request->request->all();

        $logger = $this->getServiceManager()->getLogger();
        $logger->info("REQUEST onResult(): ", $request->request->all());

        $user = $em->getRepository("AppBundle:User")->findOneBy(['email' => $requestData['shp_email']]);
        $valid = $robokassa->isValidResult($requestData);

        if ($valid) {
            $order_id = $requestData['InvId'];
            $order = $em->getRepository("AppBundle:Transaction")->findOneById($order_id);
            if ($requestData['OutSum'] != $order->getSumm()) {
                return new Response("bad order\n");
            }
            $order->setStatus("Оплачено");
            $logger->info("PAYMENT SUCCESS onResult(): ", [
                'user_email' => $requestData['shp_email'],
                'cash_add' => $requestData['OutSum'],
                'date' => $date
            ]);
            $user->setCash($user->getCash() + $requestData['OutSum']);
            $em->flush();
            $em->clear();
            return new Response($this->getSuccessAnswer($order_id));
        } else {
            return new Response("bad sign\n");
        }
    }

    private function getSuccessAnswer($inId)
    {
        return 'OK' . $inId . "\n";
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:03
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use AppBundle\Entity\SubText;
use AppBundle\Entity\Template;
use AppBundle\Form\TemplateType;
use AppBundle\Natty\Natty_TextRandomizer;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;

/**
 * Class TemplateController
 * @Route(service="app.template_controller")
 * @package AppBundle\Controller
 */
class TemplateController extends Controller
{
    use ServiceManagerSetter;
    use EntityManagerSetter;

    /**
     * @Route("/template/create", name="template_create")
     */
    public function createAction(Request $request)
    {
        $temp = new Template();
        $form = $this->getServiceManager()->getControllerAsService()->createForm(TemplateType::class, $temp, array(
            'entity_manager' => $this->getServiceManager()->getEm()->getEntityManager()
        ));
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $temp->setUser($this->getServiceManager()->getControllerAsService()->getUser());

            $em = $this->getServiceManager()->getEm()->getEntityManager();
            $em->persist($temp);
            $text = $temp->getText();
            $tRand=new Natty_TextRandomizer($text);

            for ($i=0; $i<$tRand->numVariant(); ++$i) {
                $subText=new SubText();
                $subText->setParent($temp);
                $subText->setText($tRand->getText());
               $em->persist($subText);
            }
            $em->flush();
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('template_list')
            );
        }
        return $this->getServiceManager()->getControllerAsService()->render(
            'page/template/create.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/template/list", name="template_list")
     */
    public function indexAction()
    {
        return $this->getServiceManager()->getControllerAsService()->render('page/template/list.html.twig', array());
    }

    /**
     * @Route("/template/delete", name="template_delete")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $temp=$em->getRepository("AppBundle:Template")->findOneById($request->get('id'));

        if ($temp==null || $this->getServiceManager()->getControllerAsService()->getUser() != $temp->getUser()) {
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        }
        $em->remove($temp);
        $em->flush();
        return new Response();

    }

    /**
     * @Route("/template/status", name="template_status")
     */
    public function updateStatusAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $t = $em->getRepository("AppBundle:Template")->findOneById($request->get('id'));
        if($t==null)
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        $t->setStatus(!($t->getStatus()));
        $em->flush();
        $em->clear();
        return new Response();
    }


    /**
     * @Rest\Get("/template/paginate", name="template_paginate")
     */
    public function paginateAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $em->getRepository("AppBundle:Template")->findBy(['user' => $this->getServiceManager()->getControllerAsService()->getUser()]);
        foreach ($output as &$ot){
            if($ot==null){
                return new RedirectResponse(
                    $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
                );
            }
            $ot->setUser(NULL);
            }
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json');
        return new Response($output);
    }

    /**
     * @Route("/template/update/{id}")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $templat = $em->getRepository("AppBundle:Template")->findOneById($id);
        if($templat==null){
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        }
        if ($this->getServiceManager()->getControllerAsService()->getUser() != $templat->getUser()) {
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        }

        $form = $this->getServiceManager()->getControllerAsService()->createForm(TemplateType::class, $templat, array(
            'entity_manager' => $em
        ));


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return new RedirectResponse($this->getServiceManager()->getControllerAsService()->generateUrl("template_list"));
        }

        return $this->getServiceManager()->getControllerAsService()->render(
            'page/template/update.html.twig',
            array(
                'form' => $form->createView()
            )
        );

    }


}
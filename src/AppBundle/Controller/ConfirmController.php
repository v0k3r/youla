<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 24.03.17
 * Time: 17:11
 */

namespace AppBundle\Controller;

use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use AppBundle\Entity\User;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class ConfirmController
 * @package AppBundle\Controller
 */
class ConfirmController extends Controller
{


    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }


    /**
     * @Route("/confirm/{username}/{hash}", name="confirm")
     *
     */
    public function indexAction(Request $request, $username, $hash)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(['username' => $username]);
        if ($user==null)
            return $this->render(
                'page/mail/info.html.twig',
                array(
                    'info' => "Юзера не существует"
                ));
        else if ($user->getUserRoles()->contains($em->getRepository('AppBundle:Role')->findOneByName("ROLE_USER")))
            return $this->render(
                'page/mail/info.html.twig',
                array(
                    'info' => "Этот аккаунт уже подтвержден"
                ));
        $hashKey = md5($user->getId()) . md5($user->getUsername());
        if ($hash == $hashKey) {
            $user->addUserRole($em->getRepository('AppBundle:Role')->findOneByName("ROLE_USER"));
            $user->removeUserRole($em->getRepository('AppBundle:Role')->findOneByName("ROLE_GUEST"));
            $em->flush();
            $this->authenticateUser($user);
            return $this->render(
                'page/mail/info.html.twig',
                array(
                    'info' => "Спасибо что пользуетесь нашим сервисом)"
                ));
        } else return $this->render(
        'page/mail/info.html.twig',
        array(
            'info' => "Что-то пошло не так... Обратитесь к администратору"
        )
    );

    }

}
<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 2:37
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Controller\Traits\ServiceManagerSetter;
use AppBundle\Entity\Gallery;
use AppBundle\Form\GalleryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class GalleryController
 * @Route(service="app.gallery_controller")
 * @package AppBundle\Controller
 */
class GalleryController extends Controller
{
    use ServiceManagerSetter;
    use EntityManagerSetter;

    /**
     * @Route("/gallery")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function gelleryAction(Request $request)
    {
        return $this->getServiceManager()->getControllerAsService()->render("page/gallery/gallery.html.twig");
    }


    /**
     * @Route("/gallery/create", name="gallery_create")
     */
    public function createGallery(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $gallery = new Gallery();
        $form = $this->getServiceManager()->getControllerAsService()->createForm(GalleryType::class, $gallery);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pathTo = $this->getServiceManager()->getKernelDir() . '/../web/data/galleries/' . $this->getServiceManager()->getControllerAsService()->getUser()->getId() . '/';
            /** @var UploadedFile $file */
            $file = $gallery->getPath();
            $fileName = md5(uniqid());
            $extention = $file->guessExtension();
            $file->move(
                $pathTo,
                $fileName . '.' . $extention
            );
            $pathZip = $pathTo . $fileName . '.' . $extention;
            mkdir($pathTo . $fileName);
            $zip = new ZipArchive();
            if ($zip->open($pathZip)) {
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $entry = $zip->statIndex($i);


                    if ($this->allowFileName($entry['name'])) {
                        if ($entry['size'] > 0
                            && (
                                preg_match('#\.(jpg)$#i', $entry['name'])
                                || preg_match('#\.(png)$#i', $entry['name'])
                                || preg_match('#\.(jpeg)$#i', $entry['name'])
                            )
                        ) {

                            if (preg_match('#\.(jpg)$#i', $entry['name'])) {
                                $new_filename = $fileName . $i . '.jpg';
                            } else if (preg_match('#\.(png)$#i', $entry['name'])) {
                                $new_filename = $fileName . $i . '.png';
                            } else if (preg_match('#\.(jpeg)$#i', $entry['name'])) {
                                $new_filename = $fileName . $i . '.jpeg';
                            }
                            copy('zip://' . $pathZip . '#' . $entry['name'], $pathTo . $fileName . '/' . $new_filename);
                        }
                    }
                }

                $zip->close();
            }

            unlink($pathZip);
            $gallery->setPath($fileName);
            $gallery->setUser($this->getServiceManager()->getControllerAsService()->getUser());
            $em->persist($gallery);
            $em->flush();
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('gallery_list')
            );
        }

        return $this->getServiceManager()->getControllerAsService()->render(
            'page/gallery/gallery.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    private function allowFileName($name)
    {
        return !preg_match("/[.=_+]/", substr($name, 0, 1));
    }

    function is_hidden_file($path)
    {

        $dir = "\"" . $path . "\"";
        $attr = trim(shell_exec("FOR %A IN (" . $dir . ") DO @ECHO %~aA"));
        if ($attr[3] === 'h')
            return true;

        return false;
    }

    /**
     * @Rest\Get("/gallery/paginate", name="gallery_paginate")
     */
    public function paginateAction(Request $request)
    {
        $pathFrom = $this->getServiceManager()->getKernelDir() . '/../web/data/galleries/' . $this->getServiceManager()->getControllerAsService()->getUser()->getId() . '/';
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $em->getRepository("AppBundle:Gallery")->findBy(['user' => $this->getServiceManager()->getControllerAsService()->getUser()]);
        foreach ($output as &$ot) {
            $array = array_diff(scandir($pathFrom . $ot->getPath()), array('..', '.'));
            sort($array);

            $ot->setArrays($array);
        }
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json');

        return new Response($output);
    }

    /**
     * @Route("/gallery/list", name="gallery_list")
     */
    public function indexAction()
    {

        return $this->getServiceManager()->getControllerAsService()->render('page/gallery/list.html.twig', array(
            'path' => $this->getServiceManager()->getKernelDir() . '/web/data/galleries/' . $this->getServiceManager()->getControllerAsService()->getUser()->getId()
        ));
    }

    /**
     * @Route("/gallery/delete", name="gallery_delete")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $gallery = $em->getRepository("AppBundle:Gallery")->findOneById($request->get('id'));
        if ($gallery == null || $this->getServiceManager()->getControllerAsService()->getUser() != $gallery->getUser()) {
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        }
        $em->remove($gallery);
        $em->flush();
        $dirname = $this->getServiceManager()->getKernelDir() . '/../web/data/galleries/' . $this->getServiceManager()->getControllerAsService()->getUser()->getId() . '/' . $gallery->getPath();
        array_map('unlink', glob("$dirname/*.*"));
        rmdir($dirname);

        return new Response();

    }

}
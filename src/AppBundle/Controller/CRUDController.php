<?php
// src/AppBundle/Controller/CRUDController.php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
class CRUDController extends BaseController
{
    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionMessage(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $this->admin->checkAccess('edit');
        $this->admin->checkAccess('delete');

        $modelManager = $this->admin->getModelManager();



        $selectedModels = $selectedModelQuery->execute();

        // do the merge work here


        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, ['key' => 'ae6169df86f5d4369bfecbbbe2e10db58b1db266']);
        $sparky->setOptions(['async' => false]);
        $text=$request->request->get('text');
        $title=$request->request->get('title');
        try {

            foreach ($selectedModels as $key=>$selectedModel) {
                try {
                    $results = $sparky->transmissions->post([
                        'options' => [
                            'sandbox' => true
                        ],
                        'content' => [
                            'from' => 'yoola-tool@sparkpostbox.com',
                            'subject' => $title,
                            'html' => $this->renderView('page/mail/opened.html.twig',['text'=>$text])
                        ],
                        'recipients' => [
                            ['address' => ['email'=>$selectedModel->getEmail()]]
                        ]
                    ]);

                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', 'Error');
                    return new RedirectResponse(
                        $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
                    );
                }

            }


        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', 'Error');

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        $this->addFlash('sonata_flash_success', 'All is succeed');



        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }

    public function batchActionMessageIsRelevant(array $selectedIds, $allEntitiesSelected, Request $request = null)
    {
        // here you have access to all POST parameters, if you use some custom ones
        // POST parameters are kept even after the confirmation page.
        $parameterBag = $request->request;



        $targetId = $parameterBag->get('targetId');

        // if all entities are selected, a merge can be done
        if ($allEntitiesSelected) {
            return true;
        }

        // filter out the target from the selected models
        $selectedIds = array_filter($selectedIds,
            function($selectedId) use($targetId){
                return $selectedId !== $targetId;
            }
        );

        // if at least one but not the target model is selected, a merge can be done.
        return count($selectedIds) > 0;
    }



    // ...
}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 26.03.17
 * Time: 20:52
 */

namespace AppBundle\Controller;

use AppBundle\Controller\Traits\ServiceManagerSetter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class SubTextController
 * @package AppBundle\Controller
 * @Route(service="app.subtext_controller")
 */
class SubTextController extends Controller
{

    use ServiceManagerSetter;

    /**
     * @Route("/subtext/{parentId}", name="subtext")
     */
    public function indexAction(Request $request,$parentId)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        if ($em->getRepository('AppBundle:Template')->findOneById($parentId)==null)
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('template_list')
            );

        $template=$em->getRepository('AppBundle:Template')->findOneById($parentId);
        if ($this->getServiceManager()->getControllerAsService()->getUser() != $template->getUser()) {
            return new RedirectResponse(
                $this->getServiceManager()->getControllerAsService()->generateUrl('mainpage')
            );
        }

        return $this->getServiceManager()->getControllerAsService()->render('page/template/subText/list.html.twig',
            array('template'=>$template));
    }

    /**
     * @Route("/subtext/{parentId}/paginate", name="subtext_paginate")
     */
    public function paginateAction(Request $request,$parentId)
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $output = $em->getRepository("AppBundle:SubText")->findBy(['parent'=>$parentId]);
        $output = $this->getServiceManager()->getControllerAsService()->serialize($output, 'json');
        return new Response($output);
    }

}
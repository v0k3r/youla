<?php

namespace AppBundle\Controller;

use AppBundle\Controller\Traits\ServiceManagerSetter;
use AppBundle\Entity\YoulaAccount;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use YoulaTool\YoulaBundle\ApiRequester\Youla;
use YoulaTool\YoulaBundle\ApiRequester\YoulaInterface;

/**
 * Youlaaccount controller.
 *
 * @Route("accounts", service="app.youlaaccount_controller")
 */
class YoulaAccountController extends Controller
{

    use ServiceManagerSetter;

    /**
     * Lists all youlaAccount entities.
     *
     * @Route("/", name="accounts_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getServiceManager()->getEm()->getEntityManager();

        $youlaAccounts = $em->getRepository('AppBundle:YoulaAccount')->findAll();

        return $this->getServiceManager()->getControllerAsService()->render('page/accounts/index.html.twig', array(
            'youlaAccounts' => $youlaAccounts,
        ));
    }

    /**
     * @Route("/submitPhone", name="acc-phone-submit")
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function submitPhoneAction(Request $request)
    {
        $phone = $request->get("phone");
        $lat = $request->get("lat");
        $long = $request->get("long");
        if (!$phone || !preg_match("/^9[0-9]{9}$/", $phone)) {
            return new JsonResponse([
                "status" => 0,
                "message" => "Введите номер начиная с 9..."
            ]);
        }

        $youlaApi = $this->getServiceManager()->getYoulaApi();
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $user = $this->getServiceManager()->getControllerAsService()->getUser();
        $accountLoad = $em->getRepository("AppBundle:YoulaAccount")->findOneBy([
            "user" => $user,
            "tel" => $phone
        ]);

        if (!$accountLoad) {
            $acc = $youlaApi->makeAccount($lat, $long);
            $register = $youlaApi->authByNumber($acc, '7' . $phone);
        } else {
            $register = $youlaApi->authByNumber($accountLoad->toDtoYoulaAccount(), '7' . $phone);
        }
        if (isset($register['status']) && $register['status'] == 200) {
            if (!$accountLoad && isset($acc)) {
                $em = $this->getServiceManager()->getEm()->getEntityManager();
                $yAcc = new YoulaAccount();
                $yAcc->setAdvId($acc->adv_id);
                $yAcc->setAppId($acc->app_id);
                $yAcc->setUid($acc->uid);
                $yAcc->setUsrLatitude($acc->usr_latitude);
                $yAcc->setUsrLongitude($acc->usr_longitude);
                $yAcc->setTel($phone);
                $yAcc->setStatus(0);
                $yAcc->setUser($this->getServiceManager()->getControllerAsService()->getUser());
                $em->persist($yAcc);
                $em->flush();
            }
            return new JsonResponse([
                'status' => 1
            ]);
        }
        return new JsonResponse([
            'status' => 0,
            'message' => $register['detail']
        ]);
    }

    /**
     * @Route("/confirmPhone", name="acc-phone-confirm")
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function confirmPhoneAction(Request $request)
    {
        $code = $request->get("code");
        $phone = $request->get("phone");
        if (!$phone || !$code) {
            return new JsonResponse([
                "status" => 0,
                "message" => "Укажите все поля"
            ]);
        }

        if (!preg_match("/^9[0-9]{9}$/", $phone) || !preg_match("/^[0-9]+$/", $code)) {
            return new JsonResponse([
                "status" => 0,
                "message" => "Укажите корректные данные"
            ]);
        }

        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $account = $em->getRepository("AppBundle:YoulaAccount")->findOneBy([
            'tel' => $phone
        ]);
        if (!$account) {
            return new JsonResponse([
                "status" => 0,
                "message" => "Возникла ошибка. Попробуйте еще раз"
            ]);
        }
        $api = $this->getServiceManager()->getYoulaApi();
        $youlaAccDTO = $account->toDtoYoulaAccount();
        $confirmed = $api->confirmAccount($youlaAccDTO, "7" . $phone, $code);
        if (isset($confirmed['status']) && $confirmed['status'] == 200) {
            if (isset($confirmed['data']['token'])) {
                $account->setToken($confirmed['data']['token']);
                $account->setAccId($confirmed['data']['id']);
                $em->flush($account);
            }
            if ($confirmed['data']['name'] == "" || strlen($confirmed['data']['name']) == 0) {
                //not registered yet
                return new JsonResponse([
                    'status' => 2,
                    "message" => "Ошибка"
                ]);
            } else {
                $account = $this->loadUserData($account, $api);
                $account->setStatus(1);
                $em->flush($account);
                return new JsonResponse([
                    'status' => 1,
                    "message" => "Аккаунт добавлен",
                    "account" => $account
                ]);
            }
        }
        return new JsonResponse([
            'status' => 0,
            "message" => "Ошибка"
        ]);
    }


    /**
     * @Route("/saveProfile", name="acc-profile-save")
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function saveProfileAction(Request $request)
    {
        $phone = $request->get("phone");
        $name = $request->get("name");
        $surname = $request->get("surname");
        if (!$phone || !preg_match("/^9[0-9]{9}$/", $phone)) {
            return new JsonResponse([
                "status" => 0,
                "message" => "Введите номер начиная с 9..."
            ]);
        }

        $youlaApi = $this->getServiceManager()->getYoulaApi();
        $em = $this->getServiceManager()->getEm()->getEntityManager();
        $user = $this->getServiceManager()->getControllerAsService()->getUser();
        $accountLoad = $em->getRepository("AppBundle:YoulaAccount")->findOneBy([
            "user" => $user,
            "tel" => $phone
        ]);

        if (!$accountLoad) {
            return new JsonResponse([
                'status' => 0,
                'message' => "Ошибка! Аккаунт не найден"
            ]);
        }


        $updatedResponse = $youlaApi->updateProfileInfo($accountLoad->toDtoYoulaAccount(), $surname, $name);

        if (isset($updatedResponse['status']) && $updatedResponse['status'] == 200) {
            $accountLoad->setStatus(1);
            $accountLoad->setToken($updatedResponse['data']['token']);
            $accountLoad->setSurName($updatedResponse['data']['last_name']);
            $accountLoad->setName($updatedResponse['data']['first_name']);
            $accountLoad->setDateRegistered(date("d-m-Y", $updatedResponse['data']['date_registered']));
            $accountLoad->setBonus($updatedResponse['data']["account"]['bonus_cnt']);
            $accountLoad->setBonusPromocode($updatedResponse['data']["account"]['bonus_code']);
            $accountLoad->setImage($updatedResponse['data']['image']['url']);
            $em->flush($accountLoad);
            return new JsonResponse([
                'status' => 1,
                'message' => "Сохранен!"
            ]);
        }

        return new JsonResponse([
            'status' => 0,
            'message' => "Ошибка! Аккаунт не сохранен"
        ]);
    }

    /**
     * Creates a new youlaAccount entity.
     *
     * @Route("/new", name="accounts_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $youlaAccount = new Youlaaccount();
        $form = $this->createForm('AppBundle\Form\YoulaAccountType', $youlaAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($youlaAccount);
            $em->flush($youlaAccount);

            return $this->redirectToRoute('accounts_show', array('id' => $youlaAccount->getId()));
        }

        return $this->render('page/accounts/create.html.twig', array(
            'youlaAccount' => $youlaAccount,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a youlaAccount entity.
     *
     * @Route("/{id}", name="accounts_show")
     * @Method("GET")
     */
    public function showAction(YoulaAccount $youlaAccount)
    {
        $deleteForm = $this->createDeleteForm($youlaAccount);

        return $this->render('youlaaccount/show.html.twig', array(
            'youlaAccount' => $youlaAccount,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing youlaAccount entity.
     *
     * @Route("/{id}/edit", name="accounts_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, YoulaAccount $youlaAccount)
    {
        $deleteForm = $this->createDeleteForm($youlaAccount);
        $editForm = $this->createForm('AppBundle\Form\YoulaAccountType', $youlaAccount);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('accounts_edit', array('id' => $youlaAccount->getId()));
        }

        return $this->render('youlaaccount/edit.html.twig', array(
            'youlaAccount' => $youlaAccount,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a youlaAccount entity.
     *
     * @Route("/{id}", name="accounts_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, YoulaAccount $youlaAccount)
    {
        $form = $this->createDeleteForm($youlaAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($youlaAccount);
            $em->flush($youlaAccount);
        }

        return $this->redirectToRoute('accounts_index');
    }

    /**
     * Creates a form to delete a youlaAccount entity.
     *
     * @param YoulaAccount $youlaAccount The youlaAccount entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(YoulaAccount $youlaAccount)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accounts_delete', array('id' => $youlaAccount->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function loadUserData(YoulaAccount & $user, YoulaInterface $api)
    {
        $profile = $api->getProfileInfo($user->toDtoYoulaAccount());
        $user->setBonusPromocode($profile['data']['account']['bonus_code']);
        $user->setBonus($profile['data']['account']['bonus_cnt']);
        $user->setName($profile['data']['first_name']);
        $user->setSurName($profile['data']['last_name']);
        return $user;
    }
}

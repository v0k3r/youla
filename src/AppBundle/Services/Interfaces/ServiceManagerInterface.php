<?php

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:14
 */

namespace AppBundle\Services\Interfaces;
use AppBundle\Services\RobokassaService;
use Monolog\Logger;
use YoulaTool\YoulaBundle\ApiRequester\YoulaInterface;

/**
 * Interface ServiceManagerInterface
 * @package AppBundle\Services\Interfaces
 * @property RobokassaService $robokassa
 */
interface ServiceManagerInterface
{

    /**
     * @param string $kernelDir
     * @return void
     */
    public function setKernelDir(string $kernelDir);

    /**
     * @return YoulaInterface
     */
    public function getYoulaApi();


    /**
     * @param YoulaInterface $youla
     */
    public function setYoulaApi(YoulaInterface $youla);


    /**
     * @return RobokassaService
     */
    public function getRobokassa();

    /**
     * @param RobokassaService $robokassa
     */
    public function setRobokassa(RobokassaService $robokassa);

    /**
     * @return Logger
     */
    public function getLogger();
    public function setLogger(Logger $logger);
}
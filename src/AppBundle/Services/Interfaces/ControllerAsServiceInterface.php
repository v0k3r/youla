<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:17
 */

namespace AppBundle\Services\Interfaces;

use AppBundle\Entity\User;
use FOS\RestBundle\Context\Context;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * Interface ControllerAsServiceInterface
 * @package AppBundle\Services\Interfaces
 */
interface ControllerAsServiceInterface
{

    /**
     * Security token storage setter
     * @param TokenStorage $tokenStorage Security token storage
     */
    public function setTokenStorage(TokenStorage $tokenStorage);

    /**
     * Templating engine setter
     * @param EngineInterface $templating Templating engine
     */
    public function setTemplating(EngineInterface $templating);

    /**
     * Router service setter
     * @param UrlGeneratorInterface $router Router service
     */
    public function setRouter(UrlGeneratorInterface $router);

    /**
     * Form Factory service setter
     * @param FormFactory $formFactory Form Factory service
     */
    public function setFormFactory(FormFactory $formFactory);

    /**
     * Replace base getUser() controller's method
     * @return User|null
     */
    public function getUser();

    /**
     * Replace base render() controller's method
     * @param $view
     * @param $parameters
     * @param $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($view, $parameters = array(), $response = null);

    /**
     * Replace base renderView() controller's method
     * @param $view
     * @param $parameters
     * @return string
     */
    public function renderView($view, $parameters = array());

    /**
     * Replace base stream() controller's method
     * @param $view
     * @param array $parameters
     * @param null $response
     * @return StreamedResponse
     */
    public function stream($view, $parameters = array(), $response = null);

    /**
     * Replace base generateUrl() controller's method
     * @param $route
     * @param array $params
     * @param int $referenceType
     * @return string
     */
    public function generateUrl($route, $params = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH);

    /**
     * Replace base createForm() controller's method
     * @param $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    public function createForm($type, $data = null, $options = array());

    /**
     * Replace base createForm() controller's method
     * @param null $data
     * @param array $options
     * @return FormBuilderInterface
     */
    public function createFormBuilder($data = null, $options = array());

    /**
     * Returns true if the template exists. Replace base exists() method
     * @param string|TemplateReferenceInterface $name A template name or a TemplateReferenceInterface instance
     * @return bool true if the template exists, false otherwise
     */
    public function exists($name);

    /**
     * Serialize
     * @param $object
     * @param $data string
     * @param $context Context
     * @return mixed|array
     */
    public function serialize($object,$data = 'json', $context=null);
}
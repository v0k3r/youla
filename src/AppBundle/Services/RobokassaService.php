<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Services;


/**
 */
class RobokassaService
{

    private $merchantLogin;
    private $merchantPass1;
    private $merchantPass2;
    private $merchantPassTest1;
    private $merchantPassTest2;
    private $baseUrl;
    private $isTest;

    public function buildUrl(array $paymentInfo, array $shp)
    {
        $url = $this->baseUrl . "?" . http_build_query([
                'MrchLogin' => $this->merchantLogin,
                'OutSum' => $paymentInfo['outSum'],
                'InvId' => $paymentInfo['invId'],
                'Desc' => $paymentInfo['invDesc'],
                'SignatureValue' => $this->makeSignature($paymentInfo, $shp),
                'IncCurrLabel' => isset($paymentInfo['currLabel']) ? $paymentInfo['currLabel'] : null,
                'Email' => $paymentInfo['email'],
                'Culture' => isset($paymentInfo['culture']) ? $paymentInfo['culture'] : null,
                'IsTest' => (int)$this->isTest,
            ]);
        if (!empty($shp) && ($query = http_build_query($shp)) !== '') {
            $url .= '&' . $query;
        }
        return $url;
    }

    public function isValidResult($data)
    {
        $pass = $this->getPass2();
        $out_sum = $data['OutSum'];
        $inv_id = $data['InvId'];
        $custom_param = $this->getCustomParamsString($data);
        $my_crc = strtoupper(md5("$out_sum:$inv_id:$pass".$custom_param));
        $crc = strtoupper($data['SignatureValue']);
        if ($my_crc != $crc) {
            return false;
        }
        return true;
    }


    private function getCustomParamsString(array $source)
    {
        $params = [];
        foreach ($source as $key => $val) {
            if (stripos($key, 'shp_') === 0) {
                $params[$key] = $val;
            }
        }
        ksort($params);
        $params = http_build_query($params, null, ':');
        return urldecode($params ? ':' . $params : '');
    }

    private function implodeShp($shp)
    {
        ksort($shp);
        foreach ($shp as $key => $value) {
            $shp[$key] = $key . '=' . $value;
        }
        return implode(':', $shp);
    }


    private function makeSignature($paymentInfo, $shp = [])
    {
        $signaturePrepare = "{$this->merchantLogin}:{$paymentInfo['outSum']}:{$paymentInfo['invId']}:{$this->getPass1()}";
        if (!empty($shp)) {
            $signaturePrepare .= ':' . $this->implodeShp($shp);
        }
        $sSignatureValue = md5($signaturePrepare);
        return $sSignatureValue;
    }

    /**
     * @param mixed $merchantLogin
     */
    public function setMerchantLogin($merchantLogin)
    {
        $this->merchantLogin = $merchantLogin;
    }

    /**
     * @param mixed $merchantPass1
     */
    public function setMerchantPass1($merchantPass1)
    {
        $this->merchantPass1 = $merchantPass1;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param mixed $isTest
     */
    public function setIsTest($isTest)
    {
        $this->isTest = $isTest;
    }

    /**
     * @param mixed $merchantPass2
     */
    public function setMerchantPass2($merchantPass2)
    {
        $this->merchantPass2 = $merchantPass2;
    }

    /**
     * @return mixed
     */
    public function getMerchantPassTest1()
    {
        return $this->merchantPassTest1;
    }

    /**
     * @param mixed $merchantPassTest1
     */
    public function setMerchantPassTest1($merchantPassTest1)
    {
        $this->merchantPassTest1 = $merchantPassTest1;
    }

    /**
     * @return mixed
     */
    public function getMerchantPassTest2()
    {
        return $this->merchantPassTest2;
    }

    /**
     * @param mixed $merchantPassTest2
     */
    public function setMerchantPassTest2($merchantPassTest2)
    {
        $this->merchantPassTest2 = $merchantPassTest2;
    }

    private function getPass1()
    {
        return ($this->isTest) ? $this->merchantPassTest1 : $this->merchantPass1;
    }

    private function getPass2()
    {
        return ($this->isTest) ? $this->merchantPassTest2 : $this->merchantPass2;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:18
 */

namespace AppBundle\Services;


use AppBundle\Services\Interfaces\ControllerAsServiceInterface;
use FOS\RestBundle\Context\Context;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * Class ControllerAsService
 * @package AppBundle\Services
 */
class ControllerAsService implements ControllerAsServiceInterface
{

    /**
     * $this->getUser() --> $this->tokenStorage->getToken()->getUser()
     * @var TokenStorage Security token storage
     */
    protected $tokenStorage;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * $this->render() --> $this->templating->renderResponse()
     * $this->renderView() --> $this->templating->render()
     *
     * $this->stream() -->
     *     $templating = $this->templating;
     *     $callback = function () use ($templating, $view, $parameters) {
     *         $templating->stream($view, $parameters);
     *     }
     * return new StreamedResponse($callback);
     *
     * @var EngineInterface Templating engine
     */
    protected $templating;

    /**
     * $this->generateUrl() --> $this->router->generate($route, $params, $referenceType);
     * @var UrlGeneratorInterface Router service
     */
    protected $router;

    /**
     * $this->createForm() --> $this->formFactory->create($type, $data, $options);
     * $this->createFormBuilder() --> $this->formFactory->createBuilder('form', $data, $options);
     * @var FormFactory Form Factory service
     */
    protected $formFactory;

    /**
     * Security token storage setter
     * @param TokenStorage $tokenStorage Security token storage
     */
    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Templating engine setter
     * @param EngineInterface $templating Templating engine
     */
    public function setTemplating(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * Router service setter
     * @param UrlGeneratorInterface $router Router service
     */
    public function setRouter(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Form Factory service setter
     * @param FormFactory $formFactory Form Factory service
     */
    public function setFormFactory(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * Replace base getUser() controller's method
     * @return mixed|null
     */
    public function getUser()
    {
        $user = null;
        $token = $this->tokenStorage->getToken();
        if (null !== $token && is_object($token->getUser())) {
            $user = $token->getUser();
        }
        return $user;
    }

    /**
     * Replace base render() controller's method
     * @param $view
     * @param $parameters
     * @param $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($view, $parameters = array(), $response = null)
    {
        return $this->templating->renderResponse($view, $parameters, $response);
    }

    /**
     * Replace base renderView() controller's method
     * @param $view
     * @param $parameters
     * @return string
     */
    public function renderView($view, $parameters = array())
    {
        return $this->templating->render($view, $parameters);
    }

    /**
     * Replace base stream() controller's method
     * @param $view
     * @param array $parameters
     * @param null $response
     * @return StreamedResponse
     */
    public function stream($view, $parameters = array(), $response = null)
    {
        $templating = $this->templating;
        $callback = function () use ($templating, $view, $parameters) {
            $templating->stream($view, $parameters);
        };
        return new StreamedResponse($callback);
    }

    /**
     * Replace base generateUrl() controller's method
     * @param $route
     * @param array $params
     * @param int $referenceType
     * @return string
     */
    public function generateUrl($route, $params = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->router->generate($route, $params, $referenceType);
    }

    /**
     * Replace base createForm() controller's method
     * @param $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    public function createForm($type, $data = null, $options = array())
    {
        return $this->formFactory->create($type, $data, $options);
    }

    /**
     * Replace base createForm() controller's method
     * @param null $data
     * @param array $options
     * @return FormBuilderInterface
     */
    public function createFormBuilder($data = null, $options = array())
    {
        return $this->formFactory->createBuilder(FormType::class, $data, $options);
    }

    /**
     * Returns true if the template exists. Replace base exists() method
     * @param string|TemplateReferenceInterface $name A template name or a TemplateReferenceInterface instance
     * @return bool true if the template exists, false otherwise
     */
    public function exists($name) {
        return $this->templating->exists($name);
    }

    /**
     * @return Serializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @param Serializer $serializer
     */
    public function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Serialize
     * @param $object
     * @param $data string
     * @param $context Context
     * @return mixed|array
     */
    public function serialize($object, $data = 'json', $context = null)
    {
        return $this->getSerializer()->serialize($object, $data,  $context);
    }
}
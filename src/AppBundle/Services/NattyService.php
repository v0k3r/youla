<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 26.03.17
 * Time: 18:39
 */

namespace AppBundle\Services;


require_once '../Natty/TextRandomizer.php';

class NattyService
{
    public function getRandomizeText($text)
    {
        $tRand = new \Natty_TextRandomizer($text);
        return $tRand->getText();
    }
}
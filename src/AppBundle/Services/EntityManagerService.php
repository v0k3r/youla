<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:35
 */

namespace AppBundle\Services;


use AppBundle\Services\Interfaces\EntityManagerServiceInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class EntityManagerService
 * @package AppBundle\Services
 */
class EntityManagerService implements EntityManagerServiceInterface
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


}
<?php

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12.03.2017
 * Time: 3:14
 */
namespace AppBundle\Services;

use AppBundle\Controller\Traits\ControllerAsServiceSetter;
use AppBundle\Controller\Traits\EntityManagerSetter;
use AppBundle\Services\Interfaces\ServiceManagerInterface;
use Monolog\Logger;
use YoulaTool\YoulaBundle\ApiRequester\YoulaInterface;


/**
 * Class ServiceManager
 * @package AppBundle\Services
 */
class ServiceManager implements ServiceManagerInterface
{
    use ControllerAsServiceSetter;
    use EntityManagerSetter;


    /**
     * @var string
     */
    protected $kernelDir;


    /**
     * @var YoulaInterface
     */
    protected $youlaApi;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var RobokassaService
     */
    protected $robokassa;

    /**
     * @param string $kernelDir
     */
    public function setKernelDir(string $kernelDir)
    {
        $this->kernelDir = $kernelDir;
    }

    /**
     * @return string
     */
    public function getKernelDir(): string
    {
        return $this->kernelDir;
    }

    /**
     * @return YoulaInterface
     */
    public function getYoulaApi()
    {
        return $this->youlaApi;
    }

    /**
     * @param YoulaInterface $youla
     */
    public function setYoulaApi(YoulaInterface $youla)
    {
        $this->youlaApi= $youla;
    }

    /**
     * @return RobokassaService
     */
    public function getRobokassa()
    {
        return $this->robokassa;
    }

    /**
     * @param RobokassaService $robokassa
     */
    public function setRobokassa(RobokassaService $robokassa)
    {
        $this->robokassa = $robokassa;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }



}
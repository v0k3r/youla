<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 08.03.17
 * Time: 20:11
 */

namespace AppBundle\Form;


use AppBundle\Entity\Category;
use AppBundle\Entity\Gallery;
use AppBundle\Entity\Template;

use AppBundle\Entity\YoulaAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class TemplateType
 * @package AppBundle\Form
 */
class TemplateType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['entity_manager'];
        $builder
            ->add('name', TextType::class)
            ->add('latitude', NumberType::class, array('scale' => 15))
            ->add('longitude', NumberType::class, array('scale' => 15))
            ->add('radius', IntegerType::class)
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle:Category',
            ))
            ->add('photoCount', IntegerType::class)
            ->add('title', TextareaType::class)
            ->add('text', TextareaType::class)
            ->add('responseText', TextareaType::class,['required'=>false])
            ->add('gallery', EntityType::class, array(
                'class' => Gallery::class,
                'choices' => $em->getRepository('AppBundle:Gallery')->findBy(['user' => $this->tokenStorage->getToken()->getUser()]),
                'placeholder'=>'',
                'required' =>false,


            ))
            ->add('cost', IntegerType::class)
            ->add('youlaAccount', EntityType::class, array(
                'class' => YoulaAccount::class,
                'choices' => $em->getRepository('AppBundle:YoulaAccount')->findBy(['user' => $this->tokenStorage->getToken()->getUser()]),
                'multiple' => 'true',
                'required' =>false,

            ))
            ->add('timeToPublish',EntityType::class, array(
                'class' => 'AppBundle:TimeToPublish',
                'multiple' => 'true',

            ))
            ->add('status', CheckboxType::class, ['required' => false]);

        $formModifier = function (FormInterface $form, Category $category = null) {
            $subCategory = null === $category ? array() : $category->getChild();

            $form->add('subCategory', EntityType::class, array(
                'class' => 'AppBundle:SubCategory',
                'placeholder' => '',
                'choices' => $subCategory,

            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $formModifier($event->getForm(), $data->getCategory());
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event ) use ($formModifier) {
                $event->stopPropagation();
                $category = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $category);
            },900
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('entity_manager');
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 17:50
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;


/**
 * Template
 *
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateRepository")
 */
class Template
{
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="template")
     */
    private $user;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float")
     */
    private $latitude;
    /**
     * @var float
     *
     * @ORM\Column(name="londitude", type="float")
     */
    private $longitude;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean",nullable=false )
     */
    private $status;
    /**
     * @var int
     *
     * @ORM\Column(name="radius", type="integer")
     */
    private $radius;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $category;
    /**
     * @ORM\ManyToOne(targetEntity="SubCategory")
     */
    private $subCategory;
    /**
     * @ORM\ManyToOne(targetEntity="Gallery")
     */
    private $gallery;
    /**
     * @var int
     *
     * @ORM\Column(name="phoroCount", type="integer")
     */
    private $photoCount;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;
    /**
     * @var int
     *
     * @ORM\Column(name="cost", type="integer")
     */
    private $cost;
    /**
     * @ORM\ManyToMany(targetEntity="TimeToPublish")
     */
    private $timeToPublish;


    /**
     * @var string
     *
     * @ORM\Column(name="responseText", type="text",nullable=true)
     */
    private $responseText;

    /**
     * @ORM\OneToMany(targetEntity="SubText", mappedBy="parent")
     */
    private $child;

    /**
     * @ORM\ManyToMany(targetEntity="YoulaAccount")
     * @ORM\OrderBy({"lastPublishedDate" = "ASC"})
     */
    private $youlaAccount;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->timeToPublish = new \Doctrine\Common\Collections\ArrayCollection();
        $this->youlaAccount = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return Template
     */
    public function setId($id)
    {
        $this->name = $id;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Template
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Template
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Template
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Template
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }


    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set radius
     *
     * @param integer $radius
     *
     * @return Template
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;

        return $this;
    }

    /**
     * Get radius
     *
     * @return integer
     */
    public function getRadius()
    {
        return $this->radius;
    }


    /**
     * Set photoCount
     *
     * @param integer $photoCount
     *
     * @return Template
     */
    public function setPhotoCount($photoCount)
    {
        $this->photoCount = $photoCount;

        return $this;
    }

    /**
     * Get photoCount
     *
     * @return integer
     */
    public function getPhotoCount()
    {
        return $this->photoCount;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Template
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Template
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Template
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set responseText
     *
     * @param string $responseText
     *
     * @return Template
     */
    public function setResponseText($responseText)
    {
        $this->responseText = $responseText;

        return $this;
    }

    /**
     * Get responseText
     *
     * @return string
     */
    public function getResponseText()
    {
        return $this->responseText;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Template
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Template
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set subCategory
     *
     * @param \AppBundle\Entity\SubCategory $subCategory
     *
     * @return Template
     */
    public function setSubCategory(\AppBundle\Entity\SubCategory $subCategory = null)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * Get subCategory
     *
     * @return \AppBundle\Entity\SubCategory
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * Add timeToPublish
     *
     * @param \AppBundle\Entity\TimeToPublish $timeToPublish
     *
     * @return Template
     */
    public function addTimeToPublish(\AppBundle\Entity\TimeToPublish $timeToPublish)
    {
        $this->timeToPublish[] = $timeToPublish;

        return $this;
    }

    /**
     * Remove timeToPublish
     *
     * @param \AppBundle\Entity\TimeToPublish $timeToPublish
     */
    public function removeTimeToPublish(\AppBundle\Entity\TimeToPublish $timeToPublish)
    {
        $this->timeToPublish->removeElement($timeToPublish);
    }

    /**
     * Get timeToPublish
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimeToPublish()
    {
        return $this->timeToPublish;
    }

    /**
     * Add youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     *
     * @return Template
     */
    public function addYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount[] = $youlaAccount;

        return $this;
    }

    /**
     * Remove youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     */
    public function removeYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount->removeElement($youlaAccount);
    }

    /**
     * Get youlaAccount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getYoulaAccount()
    {
        return $this->youlaAccount;
    }


    /**
     * Set gallery
     *
     * @param Gallery $gallery
     *
     * @return Template
     */
    public function setGallery(Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \AppBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\SubText $child
     *
     * @return Template
     */
    public function addChild(\AppBundle\Entity\SubText $child)
    {
        $this->child[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\SubText $child
     */
    public function removeChild(\AppBundle\Entity\SubText $child)
    {
        $this->child->removeElement($child);
    }

    /**
     * Get child
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }

    public function __toString()
    {
        return $this->name;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 10:42
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $username;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="cash", type="integer")
     */
    private $cash = 0;
    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=255)
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "Your pass must be at least {{ limit }} characters long",
     *      maxMessage = "Your pass cannot be longer than {{ limit }} characters"
     * )
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="user")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="UserMessage", mappedBy="user")
     */
    private $messages;
    /**
     * @ORM\OneToMany(targetEntity="YoulaAccount", mappedBy="user")
     */
    private $youlaAccount;
    /**
     * @ORM\OneToOne(targetEntity="UserPlan",mappedBy="user")
     */
    private $plan = NULL;
    /**
     * @ORM\OneToMany(targetEntity="Template", mappedBy="user")
     */
    private $template;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="user_role",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     *
     * @var ArrayCollection $userRoles
     */
    protected $userRoles;


    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Геттер для ролей пользователя.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    public function getSalt()
    {
        // The bcrypt algorithm doesn't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Add userRole
     *
     * @param \AppBundle\Entity\Role $userRole
     *
     * @return User
     */
    public function addUserRole(\AppBundle\Entity\Role $userRole)
    {
        $this->userRoles[] = $userRole;

        return $this;
    }

    /**
     * Remove userRole
     *
     * @param \AppBundle\Entity\Role $userRole
     */
    public function removeUserRole(\AppBundle\Entity\Role $userRole)
    {
        $this->userRoles->removeElement($userRole);
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,

            ) = unserialize($serialized);
    }

    /**
     * Геттер для массива ролей.
     *
     * @return array An array of Role objects
     */
    public function getRoles()
    {

        return $this->getUserRoles()->toArray();
    }

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->youlaAccount = new \Doctrine\Common\Collections\ArrayCollection();
        $this->template = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userRoles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set cash
     *
     * @param integer $cash
     *
     * @return User
     */
    public function setCash($cash)
    {
        $this->cash += $cash;

        return $this;
    }

    /**
     * Get cash
     *
     * @return integer
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * Add youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     *
     * @return User
     */
    public function addYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount[] = $youlaAccount;

        return $this;
    }

    /**
     * Remove youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     */
    public function removeYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount->removeElement($youlaAccount);
    }

    /**
     * Get youlaAccount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getYoulaAccount()
    {
        return $this->youlaAccount;
    }



    /**
     * Add template
     *
     * @param \AppBundle\Entity\Template $template
     *
     * @return User
     */
    public function addTemplate(\AppBundle\Entity\Template $template)
    {
        $this->template[] = $template;

        return $this;
    }

    /**
     * Remove template
     *
     * @param \AppBundle\Entity\Template $template
     */
    public function removeTemplate(\AppBundle\Entity\Template $template)
    {
        $this->template->removeElement($template);
    }

    /**
     * Get template
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set plan
     *
     * @param \AppBundle\Entity\UserPlan $plan
     *
     * @return User
     */
    public function setPlan(\AppBundle\Entity\UserPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\UserPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     *
     * @return User
     */
    public function addTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->transaction[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     */
    public function removeTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->transaction->removeElement($transaction);
    }

    /**
     * Get transaction
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    function __toString()
    {
        return $this->getUsername();
    }



    /**
     * Add message
     *
     * @param \AppBundle\Entity\UserMessage $message
     *
     * @return User
     */
    public function addMessage(\AppBundle\Entity\UserMessage $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\UserMessage $message
     */
    public function removeMessage(\AppBundle\Entity\UserMessage $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
}

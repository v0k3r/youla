<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 9.03.17
 * Time: 03:56
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * YoulaAccount
 *
 * @ORM\Table(name="youla_account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 */
class YoulaAccount
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surName", type="string", length=255, nullable=true)
     */
    private $surName;
    /**
     * @var int
     *
     * @ORM\Column(name="bonus", type="integer", nullable=true)
     */
    private $bonus;
    /**
     * @var int
     *
     * @ORM\Column(name="countAd", type="integer", nullable=true)
     */
    private $countAd;
    /**
     * @var int
     *
     * @ORM\Column(name="proxy", type="integer", nullable=true)
     */
    private $proxy;
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="youlaAccount")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="adv_id", type="string", length=255)
     */
    private $adv_id;
    /**
     * @var string
     *
     * @ORM\Column(name="app_id", type="string", length=255)
     */
    private $app_id;


    /**
     * @var string
     *
     * @ORM\Column(name="usr_latitude", type="string", length=255)
     */
    private $usr_latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_longitude", type="string", length=255, nullable=true)
     */
    private $usr_longitude;



    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;
    /**
     * @var string
     *
     * @ORM\Column(name="bonus_promocode", type="string", length=255, nullable=true)
     */
    private $bonus_promocode;

    /**
     * @var string
     *
     * @ORM\Column(name="acc_id", type="string", length=255, nullable=true)
     */
    private $acc_id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=1000, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="date_registered", type="string", length=255, nullable=true)
     */
    private $date_registered;

    /**
     * @ORM\Column(name="last_published_date", type="datetime",nullable=true)
     */

    private $lastPublishedDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->template = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return YoulaAccount
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return YoulaAccount
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surName
     *
     * @param string $surName
     *
     * @return YoulaAccount
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;

        return $this;
    }

    /**
     * Get surName
     *
     * @return string
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * Set bonus
     *
     * @param integer $bonus
     *
     * @return YoulaAccount
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return integer
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set countAd
     *
     * @param integer $countAd
     *
     * @return YoulaAccount
     */
    public function setCountAd($countAd)
    {
        $this->countAd = $countAd;

        return $this;
    }

    /**
     * Get countAd
     *
     * @return integer
     */
    public function getCountAd()
    {
        return $this->countAd;
    }

    /**
     * Set proxy
     *
     * @param integer $proxy
     *
     * @return YoulaAccount
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;

        return $this;
    }

    /**
     * Get proxy
     *
     * @return integer
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return YoulaAccount
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return YoulaAccount
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return YoulaAccount
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set advId
     *
     * @param string $advId
     *
     * @return YoulaAccount
     */
    public function setAdvId($advId)
    {
        $this->adv_id = $advId;

        return $this;
    }

    /**
     * Get advId
     *
     * @return string
     */
    public function getAdvId()
    {
        return $this->adv_id;
    }

    /**
     * Set appId
     *
     * @param string $appId
     *
     * @return YoulaAccount
     */
    public function setAppId($appId)
    {
        $this->app_id = $appId;

        return $this;
    }

    /**
     * Get appId
     *
     * @return string
     */
    public function getAppId()
    {
        return $this->app_id;
    }



    /**
     * Set usrLatitude
     *
     * @param string $usrLatitude
     *
     * @return YoulaAccount
     */
    public function setUsrLatitude($usrLatitude)
    {
        $this->usr_latitude = $usrLatitude;

        return $this;
    }

    /**
     * Get usrLatitude
     *
     * @return string
     */
    public function getUsrLatitude()
    {
        return $this->usr_latitude;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return YoulaAccount
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set bonusPromocode
     *
     * @param string $bonusPromocode
     *
     * @return YoulaAccount
     */
    public function setBonusPromocode($bonusPromocode)
    {
        $this->bonus_promocode = $bonusPromocode;

        return $this;
    }

    /**
     * Get bonusPromocode
     *
     * @return string
     */
    public function getBonusPromocode()
    {
        return $this->bonus_promocode;
    }

    /**
     * Set usrLongitude
     *
     * @param string $usrLongitude
     *
     * @return YoulaAccount
     */
    public function setUsrLongitude($usrLongitude)
    {
        $this->usr_longitude = $usrLongitude;

        return $this;
    }

    /**
     * Get usrLongitude
     *
     * @return string
     */
    public function getUsrLongitude()
    {
        return $this->usr_longitude;
    }


    public function __toString()
    {
        return $this->tel;
    }


    public function toDtoYoulaAccount()
    {
        $acc = new \YoulaTool\YoulaBundle\DTO\YoulaAccount();
        if ($this->tel)
            $acc->phone = $this->tel;
        if ($this->token)
            $acc->token = $this->token;
        if ($this->acc_id)
            $acc->acc_id = $this->acc_id;
        $acc->app_id = $this->app_id;
        $acc->adv_id = $this->adv_id;
        $acc->usr_latitude = number_format($this->usr_latitude, 7);
        $acc->usr_longitude = number_format($this->usr_longitude, 7);
        $acc->uid = $this->uid;
        return $acc;
    }

    /**
     * Set accId
     *
     * @param string $accId
     *
     * @return YoulaAccount
     */
    public function setAccId($accId)
    {
        $this->acc_id = $accId;

        return $this;
    }

    /**
     * Get accId
     *
     * @return string
     */
    public function getAccId()
    {
        return $this->acc_id;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return YoulaAccount
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dateRegistered
     *
     * @param string $dateRegistered
     *
     * @return YoulaAccount
     */
    public function setDateRegistered($dateRegistered)
    {
        $this->date_registered = $dateRegistered;

        return $this;
    }

    /**
     * Get dateRegistered
     *
     * @return string
     */
    public function getDateRegistered()
    {
        return $this->date_registered;
    }

    /**
     * Set lastPublishedDate
     *
     * @param \DateTime $lastPublishedDate
     *
     * @return YoulaAccount
     */
    public function setLastPublishedDate($lastPublishedDate)
    {
        $this->lastPublishedDate = $lastPublishedDate;

        return $this;
    }

    /**
     * Get lastPublishedDate
     *
     * @return \DateTime
     */
    public function getLastPublishedDate()
    {
        return $this->lastPublishedDate;
    }
}

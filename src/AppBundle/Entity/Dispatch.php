<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.04.17
 * Time: 17:23
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;

/**
 * Template
 *
 * @ORM\Table(name="dispatch")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DispatchRepository")
 */
class Dispatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Template")
     */
    protected $template;
    /**
     * @ORM\ManyToOne(targetEntity="YoulaAccount")
     */
    protected $youlaAccount;

    /**
     * @ORM\Column(name="published_date", type="datetime",nullable=true)
     */
    protected $publishedTime;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=255,nullable=true)
     */
    private $status;
    /**
     * @var string
     * @ORM\Column(name="link", type="string", length=255,nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="SubText")
     */
    protected $text;

    /**
     * @var int
     * @ORM\Column(name="marks", type="integer",nullable=true)
     */
    protected $marks;

    /**
     * @var int
     * @ORM\Column(name="views", type="integer",nullable=true)
     */
    protected $views;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->youlaAccount = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publishedTime
     *
     * @param \DateTime $publishedTime
     *
     * @return Dispatch
     */
    public function setPublishedTime($publishedTime)
    {
        $this->publishedTime = $publishedTime;

        return $this;
    }

    /**
     * Get publishedTime
     *
     * @return \DateTime
     */
    public function getPublishedTime()
    {
        return $this->publishedTime;
    }

    /**
     * Set template
     *
     * @param \AppBundle\Entity\Template $template
     *
     * @return Dispatch
     */
    public function setTemplate(\AppBundle\Entity\Template $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \AppBundle\Entity\Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Add youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     *
     * @return Dispatch
     */
    public function addYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount[] = $youlaAccount;

        return $this;
    }

    /**
     * Remove youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     */
    public function removeYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount)
    {
        $this->youlaAccount->removeElement($youlaAccount);
    }

    /**
     * Get youlaAccount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getYoulaAccount()
    {
        return $this->youlaAccount;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Dispatch
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set marks
     *
     * @param \int $marks
     *
     * @return Dispatch
     */
    public function setMarks($marks)
    {
        $this->marks = $marks;

        return $this;
    }

    /**
     * Get marks
     *
     * @return \int
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * Set views
     *
     * @param \int $views
     *
     * @return Dispatch
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return \int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set youlaAccount
     *
     * @param \AppBundle\Entity\YoulaAccount $youlaAccount
     *
     * @return Dispatch
     */
    public function setYoulaAccount(\AppBundle\Entity\YoulaAccount $youlaAccount = null)
    {
        $this->youlaAccount = $youlaAccount;

        return $this;
    }

    /**
     * Set text
     *
     * @param \AppBundle\Entity\SubText $text
     *
     * @return Dispatch
     */
    public function setText(\AppBundle\Entity\SubText $text = null)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return \AppBundle\Entity\SubText
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Dispatch
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
}

<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Entity;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

/**
 * Class TransactionCustomIdGeneratorStrategy
 * @package AppBundle\Entity
 */
class TransactionCustomIdGeneratorStrategy extends AbstractIdGenerator
{

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager|EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return mixed
     */
    public function generate(EntityManager $em, $entity)
    {
        $id = $highest_id = $em->createQueryBuilder()
            ->select('MAX(e.id)')
            ->from('AppBundle:Transaction', 'e')
            ->getQuery()
            ->getSingleScalarResult();
        return $id ? ++$id : 17000;
    }
}
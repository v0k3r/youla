<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 10:31
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 * @ORM\Table(name="category_parent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @UniqueEntity(fields="name", message="This name already taken")
 */
class Category
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="SubCategory", mappedBy="parent")
     */
    private $child;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->child = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add child
     * @param \AppBundle\Entity\SubCategory $child
     * @return Category
     */
    public function addChild(\AppBundle\Entity\SubCategory $child)
    {
        $this->child[] = $child;
        return $this;
    }

    /**
     * Remove child
     * @param \AppBundle\Entity\SubCategory $child
     */
    public function removeChild(\AppBundle\Entity\SubCategory $child)
    {
        $this->child->removeElement($child);
    }

    /**
     * Get child
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }
}

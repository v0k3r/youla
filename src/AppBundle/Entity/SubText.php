<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 26.03.17
 * Time: 20:28
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * SubText
 *
 * @ORM\Table(name="child_text")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubTextRepository")
 */
class SubText
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Template", inversedBy="child")
     */
    private $parent;
    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private $text;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return SubText
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Template $parent
     *
     * @return SubText
     */
    public function setParent(\AppBundle\Entity\Template $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Template
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function __toString()
    {
        return $this->text;
    }
}

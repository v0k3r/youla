<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 15:53
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rate
 * @ORM\Table(name="plan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlanRepository")
 * @UniqueEntity(fields="name", message="This name already taken")
 */
class Plan
{




    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="maxMessages", type="integer")
     */
    private $maxMessages;

    /**
     * @var int
     * @ORM\Column(name="countPromo", type="integer")
     */
    private $countPromo;

    /**
     * @var int
     * @ORM\Column(name="photoCount", type="integer")
     */
    private $photoCount;

    /**
     * @var boolean
     * @ORM\Column(name="useBonus", type="boolean")
     */
    private $useBonus;

    /**
     * @var int
     * @ORM\Column(name="proxyCount", type="integer")
     */
    private $proxyCount;

    /**
     * @var int
     * @ORM\Column(name="countAccount", type="integer")
     */
    private $countAccount;

    /**
     * @var int
     * @ORM\Column(name="time", type="integer")
     */
    private $time;

    /**
     * @var int
     * @ORM\Column(name="cost", type="integer")
     */
    private $cost;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Plan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set maxMessages
     * @param string $maxMessages
     * @return Plan
     */
    public function setMaxMessages($maxMessages)
    {
        $this->maxMessages = $maxMessages;

        return $this;
    }

    /**
     * Get maxMessages
     *
     * @return string
     */
    public function getMaxMessages()
    {
        return $this->maxMessages;
    }

    /**
     * Set countPromo
     *
     * @param integer $countPromo
     *
     * @return Plan
     */
    public function setCountPromo($countPromo)
    {
        $this->countPromo = $countPromo;

        return $this;
    }

    /**
     * Get countPromo
     *
     * @return integer
     */
    public function getCountPromo()
    {
        return $this->countPromo;
    }

    /**
     * Set photoCount
     *
     * @param integer $photoCount
     *
     * @return Plan
     */
    public function setPhotoCount($photoCount)
    {
        $this->photoCount = $photoCount;

        return $this;
    }

    /**
     * Get photoCount
     *
     * @return integer
     */
    public function getPhotoCount()
    {
        return $this->photoCount;
    }

    /**
     * Set useBonus
     *
     * @param boolean $useBonus
     *
     * @return Plan
     */
    public function setUseBonus($useBonus)
    {
        $this->useBonus = $useBonus;

        return $this;
    }

    /**
     * Get useBonus
     *
     * @return boolean
     */
    public function getUseBonus()
    {
        return $this->useBonus;
    }

    /**
     * Set proxyCount
     *
     * @param integer $proxyCount
     *
     * @return Plan
     */
    public function setProxyCount($proxyCount)
    {
        $this->proxyCount = $proxyCount;

        return $this;
    }

    /**
     * Get proxyCount
     *
     * @return integer
     */
    public function getProxyCount()
    {
        return $this->proxyCount;
    }

    /**
     * Set countAccount
     *
     * @param integer $countAccount
     *
     * @return Plan
     */
    public function setCountAccount($countAccount)
    {
        $this->countAccount = $countAccount;

        return $this;
    }

    /**
     * Get countAccount
     *
     * @return integer
     */
    public function getCountAccount()
    {
        return $this->countAccount;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return Plan
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Plan
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }


}

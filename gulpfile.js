var gulp = require('gulp'),
    jade = require('gulp-jade'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync');

var publicPath = 'app/Resources/public/',
    destPath = 'web/',
    nodeModules = 'node_modules/';

gulp.task('img', function () {
    return gulp.src(publicPath + 'img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlagins: [{removeViewBox: false}],
            une: [pngquant()]
        })))
        .pipe(gulp.dest(destPath + 'img'));
});

gulp.task('clear', function () {
    return cache.clearAll();
});

gulp.task('clean', function () {
    return del.sync([
        'web/css',
        'web/js',
        'web/fonts',
        'web/img'
    ]);
});


gulp.task('build', ['clean', 'img'], function () {

    var buildCss = gulp.src(publicPath + 'css/**/*')
        .pipe(gulp.dest(destPath + 'css'));

    var buildFonts = gulp.src(publicPath + 'fonts/**/*')
        .pipe(gulp.dest(destPath + 'fonts'));

    var buildJs = gulp.src(publicPath + 'js/**/*')
        .pipe(gulp.dest(destPath + 'js'));

    var vendor = gulp.src(publicPath + 'vendor/**/*')
        .pipe(gulp.dest(destPath + 'vendor/'));

    var images = gulp.src(publicPath + 'images/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlagins: [{removeViewBox: false}],
            une: [pngquant()]
        })))
        .pipe(gulp.dest(destPath + 'images'));

});
function initAccTable() {
    $('#accountsTable').DataTable({
        'ajax': {
            type: "GET",
            url: $('#accountsTable').data('url'),
            dataSrc: ""
        },
        columns: [{
            data: 'tel',
            title: 'Телефон',
            width: '100px'
        }, {
            data: 'name',
            title: 'Имя',
            width: '100px'
        }, {
            data: 'sur_name',
            title: 'Фамилия',
            width: '100px',
            orderable: false
        }, {
            title: 'Бонусов',
            data: 'bonus',
            width: '80px'
        }, {
            title: "Статус",
            data: 'status',
            render: function(data) {
                switch (data) {
                    case 0:
                        return "Не активирован";
                    case 1:
                        return "Активный";
                    case 2:
                        return "Требует подверждение";
                }
            }
        },
        {
            title: 'Действия',
            render: function(data, type, row) {
                return "<button class='btn btn-danger-outline btn-sm'>Удалить</button>" + "<button class='btn btn-info-outline btn-sm'>Изменить</button>";
            },
            width: '200px',
            orderable: false
        }]
    });
}

function initAddAccount() {
    var modal = $("#modal-add-account");
    var phone;
    $("#phone-acc-input").mask("(999) 999-9999");
    $("button#add-account-button").click(function (e) {
        e.preventDefault();
        modal.modal('show');
        switchModalTab(modal, ".tab-first");
    });

    $("button#phone-submit-acc-button").click(function (e) {
        e.preventDefault();
        var self = this;
        phone = $("#phone-acc-input").data( $.mask.dataName )();
        $.ajax({
            type: "POST",
            url: modal.data("phoneSubmit"),
            data: {
                phone: phone,
                lat: $("#accLat").val(),
                long: $("#accLong").val()
            }
        }).done(function (data) {
            if (data.status == 0) {
                showModalErrorMessage(data.message, modal);
            } else {
                switchModalTab(modal, ".tab-second");
            }
        })
    });

    $("#phone-confirm-acc-button").click(function (e) {
        e.preventDefault();
        var self = this;
        $.ajax({
            type: "POST",
            url: modal.data('phoneConfirm'),
            data: {
                phone: phone,
                code: $("#phone-code-acc-input").val()
            }
        }).done(function (data) {
            if (data.status == 1) {
                location.reload(true);
            } else if (data.status == 2) {
                switchModalTab(modal, ".tab-third");
            } else {
                showModalErrorMessage(data.message, modal);
            }
        })
    });

    $("#phone-profile-save-button").click(function (e) {
        e.preventDefault();
        var self = this;
        $.ajax({
            type: "POST",
            url: modal.data('profileUpdate'),
            data: {
                phone: phone,
                name: $("#phone-name-acc-input").val(),
                surname: $("#phone-surname-acc-input").val()
            }
        }).done(function (data) {
            if (data.status == 1) {
                location.reload(true);
            } else {
                showModalErrorMessage(data.message, modal);
            }
        })
    })



}

$(document).ready(function () {
    initAddAccount();
});

function switchModalTab($modal, newTab, withLoading) {
    withLoading = (typeof withLoading == 'undefined') ? false : withLoading;
    var $current = $modal.find('.tab-current');
    var $new = $modal.find(newTab);
    if (withLoading) {
        $new.addClass('tab-loading');
    } else {
        $new.removeClass('tab-loading');
    }
    $current.removeClass('tab-current');
    $new.addClass('tab-current');
}

function showLoaderInModalTab($modal) {
    $modal.find('.tab-current').addClass('tab-loading');
}


function showModalErrorMessage(message, modal) {
    var div = '<div class="error-message text-center">'+message+'</div>';
    var h1 = modal.find("h1");
    $(div).insertAfter(h1);
}